package com.example.ricardo.trabalho_v3.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CadastroFoto extends AppCompatActivity implements View.OnClickListener{

    private ImageView imagemPerfil;
    private Button tirarFoto;
    private Button adicionarImagem;
    private Button atualizarImagem;

    private String email_usuario;
    private Boolean up_data;

    private static final int CAM_REQUEST = 2;
    private static final int FOLDER_REQUEST = 3;
    private File imageFile;
    private String path;
    private byte[] stream;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_foto);

        imagemPerfil = (ImageView) findViewById(R.id.im_fotoPerfil);

        tirarFoto = (Button) findViewById(R.id.bt_tirarFoto);
        tirarFoto.setOnClickListener(this);

        adicionarImagem = (Button) findViewById(R.id.bt_adicionarImagem);
        adicionarImagem.setOnClickListener(this);

        atualizarImagem = (Button) findViewById(R.id.bt_atualizarImagem);
        atualizarImagem.setOnClickListener(this);

        email_usuario = this.getIntent().getExtras().getString("email_usuario");
        up_data = this.getIntent().getExtras().getBoolean("update_data");

        if(up_data){
            loadData();
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    protected void updatePicture(){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        if(path != null) {
            try {
                FileInputStream fis = new FileInputStream(path);
                stream = new byte[fis.available()];
                fis.read(stream);

                dao.open("write");
                dao.updateImageUsuario(stream, email_usuario);
                dao.close();

                fis.close();

                String encodeImage = Base64.encodeToString(stream, Base64.DEFAULT);

                updateImageUsuarioWeb(encodeImage, email_usuario);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void updateImageUsuarioWeb(final String encodedImage, final String email){
        StringRequest request = new StringRequest(Request.Method.POST, WSConfig.url_atualizar_imagem,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO, email);
                parameters.put(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO, encodedImage);

                return parameters;
            }
        };
        requestQueue.add(request);
    }

    protected  void loadData() {
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");
        Cursor c = dao.getImageByEmail(email_usuario);

        if (c.getCount() > 0)
        {
            c.moveToFirst();
            do{
                byte[] image = c.getBlob(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO));
                Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
                imagemPerfil.setImageBitmap(bmp);
            }while(c.moveToNext());
        }

        dao.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_tirarFoto: // tira foto
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES.toString());
                imageFile = new File(path, "foto.jpg");
                Intent getPictureCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                getPictureCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(getPictureCamera, CAM_REQUEST);
                break;
            case R.id.bt_adicionarImagem: // busca imagem no arquivo
                Intent getPictureFolder = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(getPictureFolder, FOLDER_REQUEST);
                break;
            case R.id.bt_atualizarImagem:
                Intent atualizaImagem = new Intent(CadastroFoto.this, Servicos.class);
                atualizaImagem.putExtra("email_usuario", email_usuario);
                updatePicture(); // sempre vai ser um update, já que está sendo inserido depois em uma linha de tabela que já existe
                if (up_data) {
                    Toast.makeText(getApplicationContext(), "Dados Cadastrais alterados com sucesso!", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Cadastro realizado com sucesso!", Toast.LENGTH_SHORT).show();
                }
                startActivity(atualizaImagem);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == CAM_REQUEST && resultCode == RESULT_OK) { // mostra foto tirada com a camera
            String filePath1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES.toString()).toString() + "/foto.jpg";
            path = filePath1;
            imagemPerfil.setImageDrawable(Drawable.createFromPath(filePath1));
        } else if(requestCode == FOLDER_REQUEST && resultCode == RESULT_OK){ // mostra foto buscada no arquivo
            Uri selectedImage = data.getData();
            String[] filePath2 = { MediaStore.Images.Media.DATA };
            Cursor c = getContentResolver().query(selectedImage, filePath2, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath2[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
            path = picturePath;
            imagemPerfil.setImageDrawable(Drawable.createFromPath(picturePath));
        }
    }

    @Override
    public void onBackPressed(){}
}
