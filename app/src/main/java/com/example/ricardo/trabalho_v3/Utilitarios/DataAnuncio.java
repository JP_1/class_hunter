package com.example.ricardo.trabalho_v3.Utilitarios;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ricardo on 14/11/16.
 */

public class DataAnuncio implements Serializable{
    // Dados da entidade usuario
    private String username;
    private String endereco;
    private String bairro;
    private String cidade;
    private String telefone;
    private byte[] imagemPerfil;
    // Dados da entidade anuncio
    private String titulo;
    private String disciplina;
    private String nivel;
    private String texto;
    private Date dataPostagem;
    private String dataPostagemStr;

    public DataAnuncio()
    {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getDataPostagem() {
        return dataPostagem;
    }

    public void setDataPostagem(Date dataPostagem) {
        this.dataPostagem = dataPostagem;
    }

    public byte[] getImagemPerfil() {
        return imagemPerfil;
    }

    public void setImagemPerfil(byte[] imagemPerfil) {
        this.imagemPerfil = imagemPerfil;
    }

    public String getDataPostagemStr() {
        return dataPostagemStr;
    }

    public void setDataPostagemStr(String dataPostagemStr) {
        this.dataPostagemStr = dataPostagemStr;
    }
}
