package com.example.ricardo.trabalho_v3.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Anuncio extends AppCompatActivity implements View.OnClickListener {

    private Spinner nDisciplinas;
    private Spinner nNivelAulas;
    private EditText ed_tituloAnuncio;
    private EditText ed_textoAnuncio;
    private Button postarAnuncio;

    private String username_usuario;
    private String email_usuario;
    SharedPreferences sp;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncio);

        email_usuario = this.getIntent().getExtras().getString("email_usuario");
        //username_usuario = getUsername(email_usuario); // pega o nome de usuario que vai entrar no anuncio, pelo e-mail do intent
        username_usuario = getUserUsername();

        nDisciplinas = (Spinner) findViewById(R.id.sp_disciplinaAnuncio);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.disciplinas, android.R.layout.simple_spinner_item);
        nDisciplinas.setAdapter(adapter2);

        nNivelAulas = (Spinner) findViewById(R.id.sp_nivelAnuncio);
        ArrayAdapter adapter3 = ArrayAdapter.createFromResource(this, R.array.nivelAula, android.R.layout.simple_spinner_item);
        nNivelAulas.setAdapter(adapter3);

        postarAnuncio = (Button) findViewById(R.id.bt_postarAnuncio);
        postarAnuncio.setOnClickListener(this);

        ed_tituloAnuncio = (EditText) findViewById(R.id.txt_titulo);
        ed_textoAnuncio = (EditText) findViewById(R.id.txt_informacoes);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    protected void storeAnuncio(){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());

        Calendar c = Calendar.getInstance();
        Date data = c.getTime(); // pega a data e hora atual do sistema

        dao.open("write");
        dao.putDataAnuncio(nDisciplinas.getItemAtPosition(nDisciplinas.getSelectedItemPosition()).toString(),
                nNivelAulas.getItemAtPosition(nNivelAulas.getSelectedItemPosition()).toString(),
                ed_tituloAnuncio.getText().toString(), ed_textoAnuncio.getText().toString(), username_usuario, data);
        dao.close();

        SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        String dataString = formatoDate.format(data);

        putDataAnuncioWeb(nDisciplinas.getItemAtPosition(nDisciplinas.getSelectedItemPosition()).toString(),
                nNivelAulas.getItemAtPosition(nNivelAulas.getSelectedItemPosition()).toString(),
                ed_tituloAnuncio.getText().toString(), ed_textoAnuncio.getText().toString(), username_usuario, dataString);
    }

    protected void putDataAnuncioWeb(final String disciplina, final String nivel, final String titulo,
                                     final String texto, final String username, final String data){
        StringRequest request = new StringRequest(Request.Method.POST, WSConfig.url_criar_anuncio,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO, disciplina);
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO, nivel);
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO, titulo);
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO, texto);
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK, username);
                parameters.put(ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO, data);

                return parameters;
            }
        };
        requestQueue.add(request);
    }

    /*protected String getUsername(String email_usuario){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");
        String username = dao.getUsernameByEmail(email_usuario);
        dao.close();
        return username;
    }*/

    protected boolean validaEntradas(){
        boolean valida = true;

        String aux_titulo = ed_tituloAnuncio.getText().toString();
        String aux_texto = ed_textoAnuncio.getText().toString();

        if(aux_titulo.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(), "Campo 'Título' vazio, favor preenchê-lo",
                    Toast.LENGTH_LONG).show();
        } else if(aux_texto.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(), "Campo 'Informações' vazio, favor preenchê-lo",
                    Toast.LENGTH_LONG).show();
        }

        return valida;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_postarAnuncio:
                if(validaEntradas()) {
                    storeAnuncio();
                    Intent acessarServicos = new Intent(Anuncio.this, Servicos.class);
                    acessarServicos.putExtra("email_usuario", email_usuario);
                    startActivity(acessarServicos);
                }
                break;
            default:
                break;
        }
    }

    protected String getUserUsername(){
        sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        String sp_email = sp.getString("username",null);
        return  sp_email;
    }
}