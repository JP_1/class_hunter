package com.example.ricardo.trabalho_v3.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.ricardo.trabalho_v3.R;

public class Servicos extends AppCompatActivity implements View.OnClickListener{

    //private Button buscar;
    private Button anunciar;
    private Button meusAnuncioss;
    private Button avaliar;
    private Button minhasAvaliacoes;
    private Button alterarDados;
    private Button logoff;
    private String email_usuario;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicos);
        email_usuario = getUserEmail();

        /*buscar = (Button)findViewById(R.id.bt_buscar);
        buscar.setOnClickListener(this);
        */

        anunciar = (Button)findViewById(R.id.bt_anunciar);
        anunciar.setOnClickListener(this);

        meusAnuncioss = (Button)findViewById(R.id.bt_meusAnuncios);
        meusAnuncioss.setOnClickListener(this);

        avaliar = (Button)findViewById(R.id.bt_avaliar);
        avaliar.setOnClickListener(this);

        minhasAvaliacoes = (Button)findViewById(R.id.bt_minhasAvaliacoes);
        minhasAvaliacoes.setOnClickListener(this);

        alterarDados = (Button)findViewById(R.id.bt_alterarDados);
        alterarDados.setOnClickListener(this);

        logoff = (Button)findViewById(R.id.bt_logoff);
        logoff.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.bt_buscar:
                Intent acessarBusca = new Intent(Servicos.this, Buscar.class);
                acessarBusca.putExtra("email_usuario",email_usuario);
                startActivity(acessarBusca);
                break;*/
            case R.id.bt_anunciar:
                Intent acessarTelaAnuncio = new Intent(Servicos.this,Anuncio.class);
                acessarTelaAnuncio.putExtra("email_usuario",email_usuario);
                startActivity(acessarTelaAnuncio);
                break;
            case R.id.bt_meusAnuncios:
                Intent acessMeusAnunc = new Intent(Servicos.this, MeusAnuncioss.class);
                acessMeusAnunc.putExtra("email_usuario",email_usuario);
                startActivity(acessMeusAnunc);
                break;
            case R.id.bt_avaliar:
                Intent acessarAvaliar = new Intent(Servicos.this, Avaliar.class);
                acessarAvaliar.putExtra("email_usuario",email_usuario);
                startActivity(acessarAvaliar);
                break;
            case R.id.bt_minhasAvaliacoes:
                Intent acessarMinhasAvaliacoes = new Intent(Servicos.this, MinhasAvaliacoes.class);
                acessarMinhasAvaliacoes.putExtra("email_usuario",email_usuario);
                startActivity(acessarMinhasAvaliacoes);
                break;
            case R.id.bt_alterarDados:
                Intent acessarDadosCad = new Intent(Servicos.this, Cadastro.class);
                acessarDadosCad.putExtra("update_data",true);
                acessarDadosCad.putExtra("email_usuario",email_usuario);
                startActivity(acessarDadosCad);
                break;
            case R.id.bt_logoff:
                Intent acessarTelaInicial = new Intent(Servicos.this,TelaInicial.class);
                startActivity(acessarTelaInicial);
                finish();
                break;
            default:
                break;
        }
    }
    protected String getUserEmail(){
        sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        String sp_email = sp.getString("email",null);
        return  sp_email;
    }
    @Override
    public void onBackPressed(){}



}





