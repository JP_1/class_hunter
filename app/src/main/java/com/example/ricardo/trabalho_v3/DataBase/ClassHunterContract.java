package com.example.ricardo.trabalho_v3.DataBase;

import android.provider.BaseColumns;

/**
 * Created by ricardo on 17/10/16.
 */
public final class ClassHunterContract {
    public ClassHunterContract() {}

    public static abstract class Usuario implements BaseColumns {
        public static final String TABLE_NAME = "usuario";
        public static final String COLUMN_USERNAME_USUARIO_PK= "usernameUsuarioPK";
        public static final String COLUMN_EMAIL_USUARIO = "emailUsuario";
        public static final String COLUMN_SENHA_USUARIO = "senhaUsuario";
        public static final String COLUMN_NOME_USUARIO = "nomeUsuario";
        public static final String COLUMN_SOBRENOME_USUARIO = "sobrenomeUsuario";
        public static final String COLUMN_TELEFONE_USUARIO = "telefoneUsuario";
        public static final String COLUMN_ENDERECO_USUARIO = "enderecoUsuario";
        public static final String COLUMN_BAIRRO_USUARIO = "bairroUsuario";
        public static final String COLUMN_CIDADE_USUARIO = "cidadeUsuario";
        public static final String COLUMN_ESTADO_USUARIO = "estadoUsuario";
        public static final String COLUMN_ESCOLARIDADE_USUARIO = "escolaridadeUsuario";
        public static final String COLUMN_IMAGEM_USUARIO = "imagemUsuario";
    }

    public static abstract class AnuncioAula implements BaseColumns{
        public static final String TABLE_NAME = "anuncioDeAula";
        public static final String COLUMN_TITULO_ANUNCIO = "tituloAnuncio";
        public static final String COLUMN_TEXTO_ANUNCIO = "textoAnuncio";
        public static final String COLUMN_USERNAME_ANUNCIO_FK = "usernameAnuncioFK";
        public static final String COLUMN_DISCIPLINA_ANUNCIO = "disciplinaAnuncio";
        public static final String COLUMN_NIVEL_ANUNCIO = "nivelAnuncio";
        public static final String COLUMN_DATA_ANUNCIO = "dataAnuncio";
    }

    public static abstract class Avaliacao implements BaseColumns{
        public static final String TABLE_NAME = "avaliacao";
        public static final String COLUMN_TITULO_AVALIACAO = "tituloAvaliacao";
        public static final String COLUMN_TEXTO_AVALIACAO = "textoAvaliacao";
        public static final String COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK = "usernameAvaliadorAvaliacaoFK";
        public static final String COLUMN_USERNAME_AVALIADO_AVALIACAO_FK = "usernameAvaliadoAvaliacaoFK";
        public static final String COLUMN_DATA_AVALIACAO = "dataAvaliacao";
        public static final String COLUMN_CLASSIFICACAO_AVALIACAO = "classificacaoAvaliacao";
    }
}
