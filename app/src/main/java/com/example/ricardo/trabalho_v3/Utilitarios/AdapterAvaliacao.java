package com.example.ricardo.trabalho_v3.Utilitarios;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ricardo.trabalho_v3.R;

/**
 * Created by ricardo on 20/11/16.
 */

public class AdapterAvaliacao extends ArrayAdapter<DataAvaliacao> {

    private int resource = 0;
    private LayoutInflater inflater;

    public AdapterAvaliacao(Context context, int resource)
    {
        super(context, resource);
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null)
        {
            viewHolder = new ViewHolder();

            view = inflater.inflate(resource, parent, false);

            viewHolder.imagemPerfil = (ImageView) view.findViewById(R.id.im_imagemList);
            viewHolder.usernameTitulo = (TextView) view.findViewById(R.id.lbl_item1);
            viewHolder.usernameAvaliado = (TextView) view.findViewById(R.id.lbl_item2);
            viewHolder.classificacao = (TextView) view.findViewById(R.id.lbl_item3);

            view.setTag(viewHolder);

            convertView = view;
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            view = convertView;
        }

        DataAvaliacao avaliacao = getItem(position);

        Bitmap bmp = BitmapFactory.decodeByteArray(avaliacao.getImagemPerfil(), 0, avaliacao.getImagemPerfil().length);
        viewHolder.imagemPerfil.setImageBitmap(bmp);

        viewHolder.usernameTitulo.setText(" " + avaliacao.getUsername() + " - " + avaliacao.getTitulo());
        viewHolder.usernameAvaliado.setText(" Avaliado: " + avaliacao.getUsernameAvaliado());
        viewHolder.classificacao.setText(" Classificação: " + avaliacao.getClassificacao());

        return view;
    }

    static class ViewHolder{
        ImageView imagemPerfil;
        TextView usernameTitulo;
        TextView usernameAvaliado;
        TextView classificacao;
    }
}
