package com.example.ricardo.trabalho_v3.Utilitarios;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.ricardo.trabalho_v3.Activities.TelaInicial;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;

public class ValidaLogin extends AppCompatActivity {

    /*private ArrayList<String> loginCred;
    private ArrayList<String> senhaCred;
    private ArrayList<String> usernameCred;*/
    private String email;
    private String senha;
    //private String username;

    //private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        email = this.getIntent().getExtras().getString("email");
        senha = this.getIntent().getExtras().getString("senha");
        String bd_senha = "oi";
        String bd_username = "oi";

        //requestQueue = Volley.newRequestQueue(getApplicationContext());

        boolean acesso = false;

        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        Cursor cursor;
        dao.open("read");
        cursor = dao.getUserDataByEmail(email);
        if(cursor.getCount() == 1) {
            if (cursor.moveToFirst()) {
                bd_senha= cursor.getString(cursor.getColumnIndex(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO));
                if(bd_senha.equals(senha)){
                    acesso = true;
                    bd_username = cursor.getString(cursor.getColumnIndex(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));
                }
            }
        }
        dao.close();

        Intent i = new Intent(this,TelaInicial.class);
        i.putExtra("usuario",bd_username);
        i.putExtra("acesso",acesso);
        setResult(Activity.RESULT_OK,i);
        finish();
    }

    /*protected Boolean autenticacao(){
        loginCred = new ArrayList<String>();
        senhaCred = new ArrayList<String>();
        usernameCred = new ArrayList<String>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, WSConfig.url_credenciais_acesso,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray users = response.getJSONArray("users");

                            for(int i = 0; i<users.length(); i++){
                                JSONObject user = users.getJSONObject(i);
                                System.out.print(user.getString(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO));

                                loginCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO));
                                senhaCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO));
                                usernameCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);

        for(int i=0; i<loginCred.size(); i++){ // senhaCred e usernameCred tem o mesmo tamanho
            if(loginCred.get(i).equals(email)){
                if(senhaCred.get(i).equals(senha)){
                    username = usernameCred.get(i);
                    return true;
                }
            }
            System.out.println("chegou aqui!!");
        }
        return false;
    }*/
}
