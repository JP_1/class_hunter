// Este módulo está sem utilização. As funções do mesmo foram implementadas no módulo MeusAnuncioss.java
// Com Exceção do filtro por disciplina, nivel e endereço

package com.example.ricardo.trabalho_v3.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.ricardo.trabalho_v3.R;

public class Buscar extends AppCompatActivity {

    private Spinner nDisciplinasB;
    private Spinner nNivelAulasB;

    private ListView nAnuncios;
    private String email_usuario;

    Button voltarMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        email_usuario = this.getIntent().getExtras().getString("email_usuario");

        nDisciplinasB = (Spinner)findViewById(R.id.sp_DisciplinaB);
        ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.disciplinas, android.R.layout.simple_spinner_item);
        nDisciplinasB.setAdapter(adapter2);

        nNivelAulasB = (Spinner)findViewById(R.id.sp_NivelB);
        ArrayAdapter adapter3 = ArrayAdapter.createFromResource(this, R.array.nivelAula, android.R.layout.simple_spinner_item);
        nNivelAulasB.setAdapter(adapter3);

        nAnuncios = (ListView) findViewById(R.id.lv_anuncios);
        //figuraMaps = (ImageView)findViewById(R.id.im_maps);

        voltarMenu = (Button)findViewById(R.id.bt_menu3);
        voltarMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Intent acessarServicos = new Intent(Buscar.this,Servicos.class);
                acessarServicos.putExtra("email_usuario",email_usuario);
                startActivity(acessarServicos);
            }
        });

    }

    @Override
    public void onBackPressed(){}
}
