package com.example.ricardo.trabalho_v3.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ricardo on 17/10/16.
 */
public class ClassHunterHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ClassHunter.db";

    public ClassHunterHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(SqlStrings.SQL_CREATE_TABLE_USUARIO);
        db.execSQL(SqlStrings.SQL_CREATE_TABLE_ANUNCIO);
        db.execSQL(SqlStrings.SQL_CREATE_TABLE_AVALIACAO);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL(SqlStrings.SQL_DELETE_TABLE_USUARIO);
        db.execSQL(SqlStrings.SQL_DELETE_TABLE_ANUNCIO);
        db.execSQL(SqlStrings.SQL_DELETE_TABLE_AVALIACAO);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void dropDb(Context context){
        context.deleteDatabase(DATABASE_NAME);
    }
}