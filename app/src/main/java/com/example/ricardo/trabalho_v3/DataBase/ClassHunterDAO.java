package com.example.ricardo.trabalho_v3.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.Utilitarios.AdapterAnuncio;
import com.example.ricardo.trabalho_v3.Utilitarios.AdapterAvaliacao;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAnuncio;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAvaliacao;

import java.util.Date;

/**
 * Created by ricardo on 19/10/16.
 */
public class ClassHunterDAO{
    SQLiteDatabase db; // criação do banco
    ClassHunterHelper mDbHelper;
    Context cont;


    public ClassHunterDAO(Context context) {
        mDbHelper = new ClassHunterHelper(context);
        cont = context;
    }

    public void open(String mode) {
        if(mode.equals("write")) {
            db = mDbHelper.getWritableDatabase();
        }
        else {
            db = mDbHelper.getReadableDatabase();
        }
    }

    public void close(){
        db.close();
    }

    public void dropDb(Context context){
        mDbHelper.dropDb(context);
    }

    //**** INSERÇÃO DE DADOS ****//
    public void putDataUsuario(String username, String email, String nome, String sobrenome, String senha,
                               String telefone, String endereco, String bairro, String cidade, String estado,
                               String escolaridade, byte[] imagem) {
        // Cria um novo mapeamento de valores, onde os nomes das colunas são as chaves


        ContentValues values = new ContentValues();

        values.put(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK, username);
        values.put(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO, email);
        values.put(ClassHunterContract.Usuario.COLUMN_NOME_USUARIO, nome);
        values.put(ClassHunterContract.Usuario.COLUMN_SOBRENOME_USUARIO, sobrenome);
        values.put(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO, senha);
        values.put(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO, telefone);
        values.put(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO, endereco);
        values.put(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO, bairro);
        values.put(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO, cidade);
        values.put(ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO, estado);
        values.put(ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO, escolaridade);
        values.put(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO, imagem);

        db.insertWithOnConflict(ClassHunterContract.Usuario.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public void putDataAnuncio(String disciplina, String nivel, String titulo, String texto, String username, Date data){
        // Cria um novo mapeamento de valores, onde os nomes das colunas são as chaves
        ContentValues values = new ContentValues();

        // O valor do ID não é inserido porque já tem uma variável _ID no objeto que insere ID automaticamente
        values.put(ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO, disciplina);
        values.put(ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO, nivel);
        values.put(ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO, titulo);
        values.put(ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO, texto);
        values.put(ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK, username); // chave estrangeira de Usuario
        values.put(ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO, data.getTime());

        db.insertWithOnConflict(ClassHunterContract.AnuncioAula.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public void putDataAvaliacao(String avaliador, String avaliado, String titulo, String texto, Date data, String classificacao){
        // Cria um novo mapeamento de valores, onde os nomes das colunas são as chaves
        ContentValues values = new ContentValues();

        // O valor do ID não é inserido porque já tem uma variável _ID no objeto que insere ID automaticamente
        values.put(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK, avaliador);
        values.put(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK, avaliado);
        values.put(ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO, titulo);
        values.put(ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO, texto);
        values.put(ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO, data.getTime());
        values.put(ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO, classificacao);

        db.insertWithOnConflict(ClassHunterContract.Avaliacao.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    //**** RECUPERAÇÃO DE DADOS ****//
    public Cursor getUserDataByEmail(String email)
    {
        String query = SqlStrings.SQL_SELECT_USER_BY_EMAIL + "'" + email + "'";
        Cursor c = db.rawQuery(query,null);
        return c;
    }

    public Cursor getImageByEmail(String email)
    {
        String query = SqlStrings.SQL_SELECT_IMAGE_BY_EMAIL + "'" + email + "'";
        Cursor c = db.rawQuery(query,null);
        return c;
    }

    public Cursor getUserDataByUsername(String username){
        String query = SqlStrings.SQL_SELECT_USER_BY_USERNAME + "'" + username + "'";
        Cursor c = db.rawQuery(query,null);
        return c;
    }

    public Cursor getAnuncioDataByUsername(String username){
        String query = SqlStrings.SQL_SELECT_ANUNCIOS_USUARIO + "'" + username + "'";
        Cursor c = db.rawQuery(query, null);
        return c;
    }

    public Cursor getAnuncioData(){
        String query = SqlStrings.SQL_SELECT_ANUNCIOS_GERAL;
        Cursor c = db.rawQuery(query, null);
        return c;
    }

    public Cursor getAvaliacaoDataByUsername(String username){
        String query = SqlStrings.SQL_SELECT_AVALIACAO_USUARIO + "'" + username + "'";
        Cursor c = db.rawQuery(query, null);
        return c;
    }

    public Cursor getAvaliacaoData(){
        String query = SqlStrings.SQL_SELECT_AVALIACAO_GERAL;
        Cursor c = db.rawQuery(query, null);
        return c;
    }

    public Cursor getUsernameUsuariosAvaliados(String username){
        String query = SqlStrings.SQL_SELECT_USER_BY_USERNAME_DIF + "'" + username + "'";
        Cursor c = db.rawQuery(query, null);
        return c;
    }

    public void updateDataCadastro(String username,String senha, String telefone, String endereco,
                                   String bairro, String cidade, String estado, String escolaridade){
        ContentValues cv = new ContentValues();
        cv.put(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO,senha);
        cv.put(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO,telefone);
        cv.put(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO,endereco);
        cv.put(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO,bairro);
        cv.put(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO,cidade);
        cv.put(ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO,estado);
        cv.put(ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO,escolaridade);

        db.updateWithOnConflict(ClassHunterContract.Usuario.TABLE_NAME,cv,
                ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = " + "'" + username + "'",
                null,SQLiteDatabase.CONFLICT_IGNORE);
    }

    public void updateImageUsuario(byte[] imagem, String email)
    {
        ContentValues values = new ContentValues();

        values.put(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO, imagem);

        db.updateWithOnConflict(ClassHunterContract.Usuario.TABLE_NAME, values,
                ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO + " = " + "'" + email + "'",
                null, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public String getUsernameByEmail(String email_usuario){
        Cursor c = getUserDataByEmail(email_usuario);
        String col_names[] = c.getColumnNames();
        c.moveToFirst();

        String username = null;

        for(int i = 0; i < col_names.length; i++)
        {
            switch (col_names[i])
            {
                case ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK:
                    username = c.getString(c.getColumnIndexOrThrow(col_names[i]));
                    break;
                default:
                    break;
            }
        }
        return username;
    }

    public AdapterAnuncio getAnuncios(Context context, String email, String seleciona)
    {
        AdapterAnuncio adpAnuncios = new AdapterAnuncio(context, R.layout.linha_anuncio_avaliacao);
        String username = getUsernameByEmail(email);

        Cursor c;

        if(seleciona.equals("geral")) {
            c = getAnuncioData();
        }
        else{
            c = getAnuncioDataByUsername(username);
        }

        if (c.getCount() > 0 )
        {
            c.moveToFirst();
            do {
                DataAnuncio dados = new DataAnuncio();

                dados.setTitulo(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO)));
                dados.setDisciplina(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO)));
                dados.setNivel(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO)));
                dados.setTexto(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO)));
                dados.setDataPostagem(new Date(c.getLong(c.getColumnIndexOrThrow(ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO))));
                dados.setUsername(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK)));
                dados.setEndereco(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO)));
                dados.setBairro(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO)));
                dados.setCidade(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO)));
                dados.setTelefone(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO)));
                dados.setImagemPerfil(c.getBlob(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO)));

                adpAnuncios.add(dados);
            } while (c.moveToNext());
        }

        return adpAnuncios;
    }

    public AdapterAvaliacao getAvaliacoes(Context context, String email, String seleciona)
    {
        AdapterAvaliacao adpAvaliacoes = new AdapterAvaliacao(context, R.layout.linha_anuncio_avaliacao);
        String username = getUsernameByEmail(email);

        Cursor c;

        if(seleciona.equals("geral")){
            c = getAvaliacaoData();
        }
        else{
            c = getAvaliacaoDataByUsername(username);
        }

        if (c.getCount() > 0)
        {
            c.moveToFirst();
            do {
                DataAvaliacao dados = new DataAvaliacao();

                dados.setUsername(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK)));
                dados.setUsernameAvaliado(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK)));
                dados.setImagemPerfil(c.getBlob(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO)));
                dados.setTitulo(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO)));
                dados.setTexto(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO)));
                dados.setDataPostagem(new Date(c.getLong(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO))));
                dados.setClassificacao(c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO)));

                adpAvaliacoes.add(dados);
            } while (c.moveToNext());
        }

        return adpAvaliacoes;
    }

    public  ArrayAdapter<String> getUsernameAvaliacaoUsuarios(Context context, String email)
    {
        ArrayAdapter<String> adpAvaliacoes = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        String username = getUsernameByEmail(email);

        Cursor c = getUsernameUsuariosAvaliados(username);

        if (c.getCount() > 0)
        {
            c.moveToFirst();
            do {
                String str_username = c.getString(c.getColumnIndexOrThrow(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK)); // username_usuario
                adpAvaliacoes.add(str_username);
            } while (c.moveToNext());
        }

        return adpAvaliacoes;
    }
}