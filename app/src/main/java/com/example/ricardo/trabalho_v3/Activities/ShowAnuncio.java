package com.example.ricardo.trabalho_v3.Activities;

import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAnuncio;

public class ShowAnuncio extends AppCompatActivity {
    private TextView username;
    private TextView disciplina;
    private TextView nivel;
    private TextView titulo;
    private TextView texto;
    private TextView endereco;
    private TextView bairro;
    private TextView cidade;
    private TextView telefone;
    private TextView dataAnuncio;

    private DataAnuncio dados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_anuncio);

        username = (TextView) findViewById(R.id.lbl_Anunciante);
        disciplina = (TextView) findViewById(R.id.lbl_disciplina);
        nivel = (TextView) findViewById(R.id.lbl_nivel);
        titulo = (TextView) findViewById(R.id.lbl_Titulo);
        texto = (TextView) findViewById(R.id.lbl_texto);
        endereco = (TextView) findViewById(R.id.lbl_endereco);
        bairro = (TextView) findViewById(R.id.lbl_bairro);
        cidade = (TextView) findViewById(R.id.lbl_cidade);
        telefone = (TextView) findViewById(R.id.lbl_telefone);
        dataAnuncio = (TextView) findViewById(R.id.lbl_dataAnuncio);

        dados = (DataAnuncio) getIntent().getExtras().getSerializable("DataAnuncio");
        preencheCampos();

    }

    protected void preencheCampos()
    {
        username.setText(" " + dados.getUsername());
        disciplina.setText(" " + dados.getDisciplina());
        nivel.setText(" " + dados.getNivel());
        titulo.setText(dados.getTitulo());
        texto.setText(dados.getTexto());
        endereco.setText(dados.getEndereco());
        bairro.setText(dados.getBairro());
        cidade.setText(dados.getCidade());
        telefone.setText(dados.getTelefone());

        if(dados.getDataPostagem() == null){
            dataAnuncio.setText("Postado em " + dados.getDataPostagemStr());
        }
        else {
            SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
            dataAnuncio.setText("Postado em " + formatoDate.format(dados.getDataPostagem()));
        }
    }
}
