package com.example.ricardo.trabalho_v3.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class Cadastro2 extends AppCompatActivity implements View.OnClickListener{

    private EditText ed_telefone;
    private EditText ed_logradouro;
    private EditText ed_bairro;
    private EditText ed_cidade;
    private EditText ed_estado;
    private Spinner nEscolaridade;

    private Button imgPerfil;
    private String email_usuario;

    private String email = null;
    private String username = null;
    private String nome = null;
    private String sobrenome = null;
    private String senha = null;

    private boolean up_data;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro2);

        // link com os widgets da tela
        ed_telefone = (EditText) findViewById(R.id.txt_telefone);
        ed_logradouro = (EditText) findViewById(R.id.txt_logradouro);

        ed_bairro = (EditText) findViewById(R.id.txt_bairro);
        ed_cidade = (EditText) findViewById(R.id.txt_cidade);
        ed_estado = (EditText) findViewById(R.id.txt_estado);

        nEscolaridade = (Spinner) findViewById(R.id.sp_escolaridade);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.escolaridade, android.R.layout.simple_spinner_item);
        nEscolaridade.setAdapter(adapter);

        up_data = this.getIntent().getExtras().getBoolean("update_data",false);

        if(up_data){
            email_usuario = this.getIntent().getExtras().getString("email_usuario");
            TextView tv_title = (TextView) findViewById(R.id.lbl_passo2);
            tv_title.setText("Alteração de dados cadastrais");
            loadData();
        }

        imgPerfil = (Button) findViewById(R.id.bt_imagemPerfil);
        imgPerfil.setOnClickListener(this);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    protected void getDataCadastro1(){
        email = this.getIntent().getExtras().getString("email");
        username = this.getIntent().getExtras().getString("username");
        nome = this.getIntent().getExtras().getString("nome");
        sobrenome = this.getIntent().getExtras().getString("sobrenome");
        senha = this.getIntent().getExtras().getString("senha");
    }

    protected void storeCadastro(){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pic2); // imagem default de usuario
        ByteArrayOutputStream stream0 = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream0);
        byte[] stream = stream0.toByteArray();

        // inserção no banco local
        dao.open("write");
        dao.putDataUsuario(username,email,nome,sobrenome,senha,ed_telefone.getText().toString(),
                ed_logradouro.getText().toString(),ed_bairro.getText().toString(),
                ed_cidade.getText().toString(), ed_estado.getText().toString(),
                nEscolaridade.getItemAtPosition(nEscolaridade.getSelectedItemPosition()).toString(),
                stream);
        dao.close();

        String encodeImage = Base64.encodeToString(stream, Base64.DEFAULT);

        putDataUsuarioWeb(username,email,nome,sobrenome,senha,ed_telefone.getText().toString(),
                ed_logradouro.getText().toString(),ed_bairro.getText().toString(),
                ed_cidade.getText().toString(), ed_estado.getText().toString(),
                nEscolaridade.getItemAtPosition(nEscolaridade.getSelectedItemPosition()).toString(), encodeImage);
    }

    protected void putDataUsuarioWeb(final String username, final String email, final String nome,
                                     final String sobrenome, final String senha, final String telefone,
                                     final String endereco, final String bairro, final String cidade,
                                     final String estado, final String escolaridade, final String imagem){
        StringRequest request = new StringRequest(Request.Method.POST, WSConfig.url_criar_usuario,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK, username);
                parameters.put(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO, email);
                parameters.put(ClassHunterContract.Usuario.COLUMN_NOME_USUARIO, nome);
                parameters.put(ClassHunterContract.Usuario.COLUMN_SOBRENOME_USUARIO, sobrenome);
                parameters.put(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO, senha);
                parameters.put(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO, telefone);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO, endereco);
                parameters.put(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO, bairro);
                parameters.put(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO, cidade);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO, estado);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO, escolaridade);
                parameters.put(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO, imagem);

                return parameters;
            }
        };
        requestQueue.add(request);
    }


    protected  void loadData() {
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");
        Cursor c = dao.getUserDataByEmail(email_usuario);
        String col_names[] = c.getColumnNames();
        c.moveToFirst();


        for (int i = 0; i < col_names.length; i++) {
            switch (col_names[i]) {
                case ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO:
                    ed_telefone.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    break;
                case ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO://LORGADOURO == ENDERENCO NO BANCO
                    ed_logradouro.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    break;
                case ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO:
                    ed_bairro.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                case ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO:
                    ed_cidade.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    break;
                case ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO:
                    ed_estado.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    break;
                case ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO:
                    int spinner_len= nEscolaridade.getAdapter().getCount();
                    String escolaridade_toSet = c.getString(c.getColumnIndexOrThrow(col_names[i]));

                    for(int j = 0;  j < spinner_len; j++) {
                        if(nEscolaridade.getAdapter().getItem(j).toString().equals(escolaridade_toSet)){
                            nEscolaridade.setSelection(j);
                        }
                    }
                default:
                    break;
            }
        }
        dao.close();
    }

    protected  void updateCadastro(){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("write");
        dao.updateDataCadastro(username,senha,ed_telefone.getText().toString(),
                ed_logradouro.getText().toString(),ed_bairro.getText().toString(),
                ed_cidade.getText().toString(),ed_estado.getText().toString(),
                nEscolaridade.getItemAtPosition(nEscolaridade.getSelectedItemPosition()).toString());
        dao.close();

        updateDataCadastroWeb(username,senha,ed_telefone.getText().toString(),
                ed_logradouro.getText().toString(),ed_bairro.getText().toString(),
                ed_cidade.getText().toString(),ed_estado.getText().toString(),
                nEscolaridade.getItemAtPosition(nEscolaridade.getSelectedItemPosition()).toString());
    }

    protected void updateDataCadastroWeb(final String username, final String senha, final String telefone,
                                         final String endereco, final String bairro, final String cidade,
                                         final String estado, final String escolaridade){
        StringRequest request = new StringRequest(Request.Method.POST, WSConfig.url_atualizar_usuario,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK, username);
                parameters.put(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO, senha);
                parameters.put(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO, telefone);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO, endereco);
                parameters.put(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO, bairro);
                parameters.put(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO, cidade);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO, estado);
                parameters.put(ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO, escolaridade);

                return parameters;
            }
        };
        requestQueue.add(request);
    }

    protected boolean validaEntradas(){
        boolean valida = true;

        String aux_telefone = ed_telefone.getText().toString();
        String aux_logradouro = ed_logradouro.getText().toString();
        String aux_bairro = ed_bairro.getText().toString();
        String aux_cidade = ed_cidade.getText().toString();
        String aux_estado = ed_estado.getText().toString();

        if(aux_telefone.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(),"Campo 'Telefone' vazio, favor preenche-lo",
                    Toast.LENGTH_LONG).show();
        }else if(aux_logradouro.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(),"Campo 'Endereço' vazio, favor preenche-lo",
                    Toast.LENGTH_LONG).show();

        }else if(aux_bairro.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(),"Campo 'Bairro' vazio, favor preenche-lo",
                    Toast.LENGTH_LONG).show();

        }else if(aux_cidade.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(),"Campo 'Cidade' vazio, favor preenche-lo",
                    Toast.LENGTH_LONG).show();

        }else if(aux_estado.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(),"Campo 'Estado' vazio, favor preenche-lo",
                    Toast.LENGTH_LONG).show();
        }
        return valida;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_imagemPerfil:
                if(validaEntradas()) {
                    getDataCadastro1();
                    Intent imagemPerfil = new Intent(Cadastro2.this, CadastroFoto.class);
                    imagemPerfil.putExtra("email_usuario", email);
                    imagemPerfil.putExtra("update_data", up_data);
                    updateUserSp();
                    if (up_data) {
                        updateCadastro();
                    } else {
                        storeCadastro();
                    }
                    startActivity(imagemPerfil);
                }
                break;
            default:
                break;
        }
    }
    protected void updateUserSp(){
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor sp_ed = sp.edit();
        sp_ed.putString("email",email);
        sp_ed.putString("senha",senha);
        sp_ed.commit();

    }
}