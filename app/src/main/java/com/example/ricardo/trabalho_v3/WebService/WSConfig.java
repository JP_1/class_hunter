package com.example.ricardo.trabalho_v3.WebService;

/**
 * Created by ricardo on 23/11/16.
 */

public class WSConfig {
    public static final String JSONArray_anuncios = "announcements";
    public static final String JSONArray_avaliacoes = "valuations";
    public static final String url_criar_usuario = "http://rivima.com/android/createUser.php";
    public static final String url_criar_anuncio = "http://rivima.com/android/createAnnouncement.php";
    public static final String url_criar_avaliacao = "http://rivima.com/android/createValuation.php";
    public static final String url_atualizar_usuario = "http://rivima.com/android/updateUser.php";
    public static final String url_atualizar_imagem = "http://rivima.com/android/updateImage.php";
    public static final String url_mostrar_anuncios_geral = "http://rivima.com/android/generalAnnouncements.php";
    public static final String url_mostrar_avaliacoes_geral = "http://rivima.com/android/generalValuations.php";
    public static final String url_lista_usuarios_avaliados = "http://rivima.com/android/valuatedUsers.php";
    public static final String url_credenciais_acesso = "http://rivima.com/android/validation.php";

}
