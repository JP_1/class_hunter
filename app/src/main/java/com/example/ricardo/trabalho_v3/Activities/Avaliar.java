package com.example.ricardo.trabalho_v3.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Avaliar extends AppCompatActivity implements View.OnClickListener{

    private Spinner usuarioAvaliado;
    ArrayAdapter<String> adpUsernameAvaliados;

    private RadioGroup classificacao;
    private RadioButton selecionado;
    String itemSelecionado;

    private EditText nTitulo;
    private EditText nAvaliacao;
    Button postarAvaliacao;

    private String email_usuario;
    private String username_usuario;
    SharedPreferences sp;

    private RequestQueue requestQueue;
    private RequestQueue requestQueue2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliar);
        email_usuario = this.getIntent().getExtras().getString("email_usuario");
        //username_usuario = getUsername(email_usuario);
        username_usuario = getUserUsername();

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue2 = Volley.newRequestQueue(getApplicationContext());

        usuarioAvaliado = (Spinner) findViewById(R.id.sp_usernameAvaliar);
        getUsernameUsuariosAvaliadosWeb(); // preenche o spinner
        usuarioAvaliado.setAdapter(adpUsernameAvaliados);

        classificacao = (RadioGroup) findViewById(R.id.rg_Classificacao);
        selecionado = (RadioButton) findViewById(R.id.rb_Regular); // regular por defaulf
        selecionado.setChecked(true); // Avaliaçao default como regular

        nTitulo = (EditText) findViewById(R.id.txt_tituloAvaliacao);
        nAvaliacao = (EditText) findViewById(R.id.txt_avaliacao);

        postarAvaliacao = (Button)findViewById(R.id.bt_postarAvaliacao);
        postarAvaliacao.setOnClickListener(this);
    }

    protected void storeAvaliacao(){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());

        Calendar c = Calendar.getInstance();
        Date data = c.getTime(); // pega a data e hora atual do sistema

        itemSelecionado = getClassificacao();

        dao.open("write");
        dao.putDataAvaliacao(username_usuario, usuarioAvaliado.getItemAtPosition(usuarioAvaliado.getSelectedItemPosition()).toString(),
                nTitulo.getText().toString(), nAvaliacao.getText().toString(), data, itemSelecionado);
        dao.close();

        SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        String dataString = formatoDate.format(data);

        putDataAvaliacaoWeb(username_usuario, usuarioAvaliado.getItemAtPosition(usuarioAvaliado.getSelectedItemPosition()).toString(),
                nTitulo.getText().toString(), nAvaliacao.getText().toString(), dataString, itemSelecionado);
    }

    protected void putDataAvaliacaoWeb(final String avaliador, final String avaliado, final String titulo,
                                       final String texto, final String data, final String classificacao){
        StringRequest request = new StringRequest(Request.Method.POST, WSConfig.url_criar_avaliacao,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK, avaliador);
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK, avaliado);
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO, titulo);
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO, texto);
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO, data);
                parameters.put(ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO, classificacao);

                return parameters;
            }
        };
        requestQueue.add(request);
    }

    /*protected String getUsername(String email_usuario){
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");

        String username = dao.getUsernameByEmail(email_usuario);
        dao.close();
        return username;
    }*/

    /*protected void getUsernameUsuariosAvaliadoss()
    {
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");
        adpUsernameAvaliados = dao.getUsernameAvaliacaoUsuarios(this, email_usuario);
        usuarioAvaliado.setAdapter(adpUsernameAvaliados);
        dao.close();
    }*/

    protected String getClassificacao(){
        String item;
        selecionado = (RadioButton) findViewById(classificacao.getCheckedRadioButtonId());
        item = selecionado.getText().toString();
        return item;
    }

    protected boolean validaEntradas(){
        boolean valida = true;

        String aux_titulo = nTitulo.getText().toString();
        String aux_texto = nAvaliacao.getText().toString();

        if(adpUsernameAvaliados.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(), "Ainda não há usuários a serem avaliados",
                    Toast.LENGTH_LONG).show();
        } else if(aux_titulo.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(), "Campo 'Título' vazio, favor preenchê-lo",
                    Toast.LENGTH_LONG).show();
        } else if(aux_texto.isEmpty()){
            valida = false;
            Toast.makeText(getApplicationContext(), "Campo 'Comentários' vazio, favor preenchê-lo",
                    Toast.LENGTH_LONG).show();
        }else if(usuarioAvaliado.getItemAtPosition(usuarioAvaliado.getSelectedItemPosition()).toString().equals(getUserUsername())){
            valida = false;
            Toast.makeText(getApplicationContext(), "O usuário não pode avaliar a si mesmo. Favor escolher outro",
                    Toast.LENGTH_LONG).show();
        }

        return valida;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_postarAvaliacao:
                if(validaEntradas()){
                    storeAvaliacao();
                    Intent postarAv = new Intent(Avaliar.this, Servicos.class);
                    postarAv.putExtra("email_usuario", email_usuario);
                    startActivity(postarAv);
                }
                break;
            default:
                break;
        }
    }

    protected String getUserUsername(){
        sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        String sp_email = sp.getString("username",null);
        return  sp_email;
    }

    protected void getUsernameUsuariosAvaliadosWeb(){
        adpUsernameAvaliados = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, WSConfig.url_lista_usuarios_avaliados,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray users = response.getJSONArray("users");

                            for(int i = 0; i<users.length(); i++){
                                JSONObject user = users.getJSONObject(i);
                                //System.out.println(user.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));

                                adpUsernameAvaliados.add(user.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue2.add(jsonObjectRequest);
    }
}