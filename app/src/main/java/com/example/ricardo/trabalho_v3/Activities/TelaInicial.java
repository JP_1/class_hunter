package com.example.ricardo.trabalho_v3.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class TelaInicial extends AppCompatActivity implements View.OnClickListener {
    private final int valida_id = 1;

    private EditText ed_email;
    private EditText ed_senha;
    private Button entrar;
    private Button cadastro;

    private ArrayList<String> loginCred;
    private ArrayList<String> senhaCred;
    private ArrayList<String> usernameCred;

    SharedPreferences sp;

    private String username;

    private RequestQueue requestQueue;

    private Boolean verifica = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);

        sp = getSharedPreferences("user",Context.MODE_PRIVATE);

        ed_email = (EditText)findViewById(R.id.txt_Email);
        ed_senha = (EditText)findViewById(R.id.txt_senha);

        entrar = (Button) findViewById(R.id.bt_entrar);
        entrar.setOnClickListener(this);

        cadastro = (Button) findViewById(R.id.bt_cadastro);
        cadastro.setOnClickListener(this);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == valida_id){
            if(resultCode == Activity.RESULT_OK){
                boolean acesso = data.getBooleanExtra("acesso",false);
                if(acesso == true){
                    updateUserSp();
                    Toast.makeText(this,"Bem Vindo " + data.getStringExtra("usuario"),Toast.LENGTH_SHORT).show();
                    Intent acessarMenu = new Intent(TelaInicial.this,Servicos.class);
                    startActivity(acessarMenu);
                }else {
                    Toast.makeText(this,"Email e/ou Senha Incorretos",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }*/

    /*public void autenticar(View v){
        Intent i = new Intent(this,ValidaLogin.class);
        i.putExtra("email",ed_email.getText().toString());
        i.putExtra("senha",ed_senha.getText().toString());
        startActivityForResult(i,valida_id);
    }*/

    protected Boolean autentica(){
        loginCred = new ArrayList<String>();
        senhaCred = new ArrayList<String>();
        usernameCred = new ArrayList<String>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, WSConfig.url_credenciais_acesso,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray users = response.getJSONArray("users");

                            for(int i = 0; i<users.length(); i++){
                                JSONObject user = users.getJSONObject(i);
                                System.out.print(user.getString(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO));

                                if(user.getString(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO).equals(ed_email.getText().toString())){
                                    if(user.getString(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO).equals(ed_senha.getText().toString())){
                                        username = user.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK);
                                        verifica = true;
                                    }
                                }

                                //loginCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO));
                                //senhaCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO));
                                //usernameCred.add(user.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);

        /*for(int i=0; i<loginCred.size(); i++){ // senhaCred e usernameCred tem o mesmo tamanho
            if(loginCred.get(i).equals(ed_email.getText().toString())){
                if(senhaCred.get(i).equals(ed_senha.getText().toString())){
                    username = usernameCred.get(i);
                    return true;
                }
            }
            //System.out.println("chegou aqui!!");
        }*/
        return verifica;
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bt_entrar:
                //autenticar(view);
                if(autentica())
                {
                    updateUserSp();
                    Toast.makeText(this,"Bem Vindo " + username,Toast.LENGTH_SHORT).show();
                    Intent acessarMenu = new Intent(TelaInicial.this,Servicos.class);
                    startActivity(acessarMenu);
                }
                else
                {
                    Toast.makeText(this,"Email e/ou Senha Incorretos",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bt_cadastro:
                Intent acessarCadastro = new Intent(TelaInicial.this,Cadastro.class);
                acessarCadastro.putExtra("write_read",false);
                startActivity(acessarCadastro);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        getUserData();
    }

    protected void getUserData(){
        String sp_senha = sp.getString("senha",null);
        String sp_email = sp.getString("email",null);

        if(sp_senha != null && sp_email != null){
            ed_email.setText(sp_email, TextView.BufferType.EDITABLE);
            ed_senha.setText(sp_senha, TextView.BufferType.EDITABLE);
        }
    }

    protected void updateUserSp(){
        SharedPreferences.Editor sp_ed = sp.edit();
        sp_ed.putString("email",ed_email.getText().toString());
        sp_ed.putString("senha",ed_senha.getText().toString());
        sp_ed.putString("username", username);
        sp_ed.commit();

    }

    @Override
    public void onBackPressed(){}
}
