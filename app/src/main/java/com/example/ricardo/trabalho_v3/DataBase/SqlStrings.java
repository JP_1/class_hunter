package com.example.ricardo.trabalho_v3.DataBase;

/**
 * Created by ricardo on 17/10/16.
 */
public class SqlStrings {

    // criação das tabelas
    public static final String SQL_CREATE_TABLE_USUARIO =
            "CREATE TABLE IF NOT EXISTS " + ClassHunterContract.Usuario.TABLE_NAME +
                    " (" + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " TEXT PRIMARY KEY NOT NULL, " +
                    ClassHunterContract.Usuario.COLUMN_NOME_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_SOBRENOME_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_ESTADO_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_ESCOLARIDADE_USUARIO + " TEXT, " +
                    ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO + " BLOB )"; // para armazenar a imagem de perfil

    public static final String SQL_CREATE_TABLE_ANUNCIO =
            "CREATE TABLE IF NOT EXISTS " + ClassHunterContract.AnuncioAula.TABLE_NAME +
                    " (" + ClassHunterContract.AnuncioAula._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO + " TEXT, " +
                    ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO + " TEXT, " +
                    ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK + " TEXT, " +
                    ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO + " TEXT, " +
                    ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO + " TEXT, " +
                    ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO + " DATE, " +
                    "FOREIGN KEY (" + ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK + ") REFERENCES " +
                    ClassHunterContract.Usuario.TABLE_NAME + " (" + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + "))";

    public static final String SQL_CREATE_TABLE_AVALIACAO =
            "CREATE TABLE IF NOT EXISTS " + ClassHunterContract.Avaliacao.TABLE_NAME +
                    " (" + ClassHunterContract.Avaliacao._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO + " TEXT, " +
                    ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO + " TEXT, " +
                    ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK + " TEXT, " +
                    ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK + " TEXT, " +
                    ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO + " DATE, " +
                    ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO + " TEXT, " +
                    "FOREIGN KEY (" + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK + ") REFERENCES " +
                    ClassHunterContract.Usuario.TABLE_NAME + " (" + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + ")," +
                    "FOREIGN KEY (" + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK + ") REFERENCES " +
                    ClassHunterContract.Usuario.TABLE_NAME + " (" + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + ") )";

    //apagar tabelas
    public static final String SQL_DELETE_TABLE_USUARIO =
            "DROP TABLE IF EXISTS " + ClassHunterContract.Usuario.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_ANUNCIO =
            "DROP TABLE IF EXISTS " + ClassHunterContract.AnuncioAula.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_AVALIACAO =
            "DROP TABLE IF EXISTS " + ClassHunterContract.Avaliacao.TABLE_NAME;

    //Consultas
    public static final String SQL_SELECT_ANUNCIOS_GERAL = "SELECT " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula._ID + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO + " FROM " +
            ClassHunterContract.Usuario.TABLE_NAME + " INNER JOIN " + ClassHunterContract.AnuncioAula.TABLE_NAME + " ON " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK;


    public static final String SQL_SELECT_ANUNCIOS_USUARIO = "SELECT " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula._ID + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO + ", " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO + " FROM " +
            ClassHunterContract.Usuario.TABLE_NAME + " INNER JOIN " + ClassHunterContract.AnuncioAula.TABLE_NAME + " ON " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = " +
            ClassHunterContract.AnuncioAula.TABLE_NAME + "." + ClassHunterContract.AnuncioAula.COLUMN_USERNAME_ANUNCIO_FK + " AND " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = ";

    public static final String SQL_SELECT_AVALIACAO_GERAL = "SELECT " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao._ID + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO + " FROM " +
            ClassHunterContract.Usuario.TABLE_NAME + " INNER JOIN " + ClassHunterContract.Avaliacao.TABLE_NAME + " ON " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK;

    public static final String SQL_SELECT_AVALIACAO_USUARIO = "SELECT " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao._ID + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO + ", " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO + ", " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO + " FROM " +
            ClassHunterContract.Usuario.TABLE_NAME + " INNER JOIN " + ClassHunterContract.Avaliacao.TABLE_NAME + " ON " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = " +
            ClassHunterContract.Avaliacao.TABLE_NAME + "." + ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK + " AND " +
            ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = ";

    public static final String SQL_SELECT_USER_BY_EMAIL =
            "SELECT * FROM " + ClassHunterContract.Usuario.TABLE_NAME + " WHERE " +
                    ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO + " = ";

    public static final String SQL_SELECT_USER_BY_USERNAME =
            "SELECT * FROM " + ClassHunterContract.Usuario.TABLE_NAME + " WHERE " +
                    ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " = ";

    public static final String SQL_SELECT_USER_BY_USERNAME_DIF =
            "SELECT * FROM " + ClassHunterContract.Usuario.TABLE_NAME + " WHERE " +
                    ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK + " <> "; // preencher spinner usuários avaliados

    public static final String SQL_SELECT_IMAGE_BY_EMAIL =
            "SELECT " + ClassHunterContract.Usuario.TABLE_NAME + "." + ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO +
                    " FROM " + ClassHunterContract.Usuario.TABLE_NAME + " WHERE " +
                    ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO + " = ";
}
