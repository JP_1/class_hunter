package com.example.ricardo.trabalho_v3.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.Utilitarios.AdapterAvaliacao;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAvaliacao;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MinhasAvaliacoes extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener{
    private Spinner selecionaAvaliacoes;
    private Button selecionaAvaliacoesOK;

    private ListView minhasAvaliacoes;
    private AdapterAvaliacao adpAvaliacoes;

    private Button avaliar;
    private Button voltarMenu2;

    String email_usuario;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minhas_avaliacoes);

        selecionaAvaliacoes = (Spinner) findViewById(R.id.sp_selecionaAvaliacao);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.selecionaAvaliacoes, android.R.layout.simple_spinner_item);
        selecionaAvaliacoes.setAdapter(adapter);

        selecionaAvaliacoesOK = (Button) findViewById(R.id.bt_OKSelecionaAvaliacao);
        selecionaAvaliacoesOK.setOnClickListener(this);

        email_usuario = this.getIntent().getExtras().getString("email_usuario");

        minhasAvaliacoes = (ListView)findViewById(R.id.lv_minhasAvaliacoes);
        minhasAvaliacoes.setOnItemClickListener(this);

        avaliar = (Button) findViewById(R.id.bt_avaliarAvaliacoes);
        avaliar.setOnClickListener(this);

        voltarMenu2 = (Button)findViewById(R.id.bt_menu2);
        voltarMenu2.setOnClickListener(this);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    protected void getAvaliacoes(String seleciona)
    {
        getAvaliacoesWeb(seleciona);
        minhasAvaliacoes.setAdapter(adpAvaliacoes);
    }

    protected void getAvaliacoesWeb(String seleciona){
        if(seleciona.equals("geral")) {
            adpAvaliacoes = new AdapterAvaliacao(this, R.layout.linha_anuncio_avaliacao);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, WSConfig.url_mostrar_avaliacoes_geral,
                    new Response.Listener<JSONObject>(){
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray valuations = response.getJSONArray(WSConfig.JSONArray_avaliacoes);

                                for(int i = 0; i<valuations.length(); i++){
                                    JSONObject valuation = valuations.getJSONObject(i);
                                    DataAvaliacao dados = new DataAvaliacao();

                                    dados.setUsername(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADOR_AVALIACAO_FK));
                                    dados.setUsernameAvaliado(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_USERNAME_AVALIADO_AVALIACAO_FK));
                                    dados.setTitulo(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_TITULO_AVALIACAO));
                                    dados.setTexto(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_TEXTO_AVALIACAO));
                                    dados.setDataPostagemStr(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_DATA_AVALIACAO));
                                    dados.setClassificacao(valuation.getString(ClassHunterContract.Avaliacao.COLUMN_CLASSIFICACAO_AVALIACAO));

                                    String imageEncoded = valuation.getString(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO);
                                    dados.setImagemPerfil(Base64.decode(imageEncoded, Base64.DEFAULT));

                                    adpAvaliacoes.add(dados);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            requestQueue.add(jsonObjectRequest);
        } else {
            // Aqui busca no banco de dados interno
            /*ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
            dao.open("read");
            adpAvaliacoes = dao.getAvaliacoes(this, email_usuario, "usuario");
            //minhasAvaliacoes.setAdapter(adpAvaliacoes);
            dao.close();*/

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_avaliarAvaliacoes:
                Intent avaliar2 = new Intent(MinhasAvaliacoes.this, Avaliar.class);
                avaliar2.putExtra("email_usuario",email_usuario);
                startActivity(avaliar2);
                break;
            case R.id.bt_menu2:
                Intent acessarMenu2 = new Intent(MinhasAvaliacoes.this, Servicos.class);
                acessarMenu2.putExtra("email_usuario",email_usuario);
                startActivity(acessarMenu2);
                break;
            case R.id.bt_OKSelecionaAvaliacao:
                String seleciona = selecionaAvaliacoes.getItemAtPosition(selecionaAvaliacoes.getSelectedItemPosition()).toString();
                if(seleciona.equals("Todas as Avaliações")){
                    getAvaliacoes("geral");
                }
                else{
                    getAvaliacoes("usuario");
                }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed(){}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DataAvaliacao dados = adpAvaliacoes.getItem(position);

        Intent info = new Intent(MinhasAvaliacoes.this, ShowAvaliacao.class);
        info.putExtra("DataAvaliacao", dados);
        startActivity(info);
    }
}
