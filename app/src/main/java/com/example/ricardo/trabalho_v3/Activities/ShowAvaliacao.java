package com.example.ricardo.trabalho_v3.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAvaliacao;

import java.text.SimpleDateFormat;

public class ShowAvaliacao extends AppCompatActivity {

    private TextView avaliador;
    private TextView avaliado;
    private TextView classificacao;
    private TextView titulo;
    private TextView texto;
    private TextView dataAvaliacao;

    private DataAvaliacao dados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_avaliacao);

        avaliador = (TextView) findViewById(R.id.lbl_avaliador);
        avaliado = (TextView) findViewById(R.id.lbl_avaliado);
        classificacao = (TextView) findViewById(R.id.lbl_classificacao);
        titulo = (TextView) findViewById(R.id.lbl_tituloAv);
        texto = (TextView) findViewById(R.id.lbl_descricaoAv);
        dataAvaliacao = (TextView) findViewById(R.id.lbl_dataAvaliacao);

        dados = (DataAvaliacao) getIntent().getExtras().getSerializable("DataAvaliacao");
        preencheCampos();
    }

    protected void preencheCampos()
    {
        avaliador.setText(" " + dados.getUsername());
        avaliado.setText(" " + dados.getUsernameAvaliado());
        classificacao.setText(" " + dados.getClassificacao());
        titulo.setText(dados.getTitulo());
        texto.setText(dados.getTexto());

        if(dados.getDataPostagem() == null){
            dataAvaliacao.setText("Postado em " + dados.getDataPostagemStr());
        }
        else{
            SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
            dataAvaliacao.setText("Postado em " + formatoDate.format(dados.getDataPostagem()));
        }


    }
}
