package com.example.ricardo.trabalho_v3.Utilitarios;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ricardo on 14/11/16.
 */

public class DataAvaliacao implements Serializable {
    // Dados da entidade usuario
    private String username;
    private String usernameAvaliado;
    private byte[] imagemPerfil;
    // Dados da entidade avaliacao
    private String classificacao;
    private String titulo;
    private String texto;
    private Date dataPostagem;
    private String dataPostagemStr;

    public DataAvaliacao()
    {}

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameAvaliado() {
        return usernameAvaliado;
    }

    public void setUsernameAvaliado(String usernameAvaliado) {
        this.usernameAvaliado = usernameAvaliado;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getDataPostagem() {
        return dataPostagem;
    }

    public void setDataPostagem(Date dataPostagem) {
        this.dataPostagem = dataPostagem;
    }

    public byte[] getImagemPerfil() {
        return imagemPerfil;
    }

    public void setImagemPerfil(byte[] imagemPerfil) {
        this.imagemPerfil = imagemPerfil;
    }

    public String getDataPostagemStr() {
        return dataPostagemStr;
    }

    public void setDataPostagemStr(String dataPostagemStr) {
        this.dataPostagemStr = dataPostagemStr;
    }
}
