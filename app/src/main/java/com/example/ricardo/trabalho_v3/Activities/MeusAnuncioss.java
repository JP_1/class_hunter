package com.example.ricardo.trabalho_v3.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.R;
import com.example.ricardo.trabalho_v3.Utilitarios.AdapterAnuncio;
import com.example.ricardo.trabalho_v3.Utilitarios.DataAnuncio;
import com.example.ricardo.trabalho_v3.WebService.WSConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MeusAnuncioss extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Spinner selecionaAnuncios;
    private Button selecionaAnunciosOK;

    private ListView meusAnuncioss;
    private AdapterAnuncio adpAnuncios;

    private Button criarAnuncio;
    private Button voltarMenu;

    private String email_usuario;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meus_anuncioss);

        selecionaAnuncios = (Spinner) findViewById(R.id.sp_selecionaAnuncio);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.selecionaAnuncios, android.R.layout.simple_spinner_item);
        selecionaAnuncios.setAdapter(adapter);

        selecionaAnunciosOK = (Button) findViewById(R.id.bt_OKSelecionaAnuncio);
        selecionaAnunciosOK.setOnClickListener(this);

        email_usuario = this.getIntent().getExtras().getString("email_usuario");

        meusAnuncioss = (ListView) findViewById(R.id.lv_meusAnuncios);
        meusAnuncioss.setOnItemClickListener(this);

        criarAnuncio = (Button) findViewById(R.id.bt_criarAnuncio);
        criarAnuncio.setOnClickListener(this);

        voltarMenu = (Button) findViewById(R.id.bt_menu);
        voltarMenu.setOnClickListener(this);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    protected void getAnuncios(String seleciona) {
        getAnunciosWebLocal(seleciona);
        meusAnuncioss.setAdapter(adpAnuncios);
    }

    protected void getAnunciosWebLocal(String seleciona){
        if(seleciona.equals("geral")) {
            adpAnuncios = new AdapterAnuncio(this, R.layout.linha_anuncio_avaliacao);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, WSConfig.url_mostrar_anuncios_geral,
                    new Response.Listener<JSONObject>(){
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray announcements = response.getJSONArray(WSConfig.JSONArray_anuncios);

                                for(int i = 0; i<announcements.length(); i++){
                                    JSONObject announcement = announcements.getJSONObject(i);
                                    DataAnuncio dados = new DataAnuncio();

                                    dados.setTitulo(announcement.getString(ClassHunterContract.AnuncioAula.COLUMN_TITULO_ANUNCIO));
                                    dados.setDisciplina(announcement.getString(ClassHunterContract.AnuncioAula.COLUMN_DISCIPLINA_ANUNCIO));
                                    dados.setNivel(announcement.getString(ClassHunterContract.AnuncioAula.COLUMN_NIVEL_ANUNCIO));
                                    dados.setTexto(announcement.getString(ClassHunterContract.AnuncioAula.COLUMN_TEXTO_ANUNCIO));
                                    dados.setDataPostagemStr(announcement.getString(ClassHunterContract.AnuncioAula.COLUMN_DATA_ANUNCIO));
                                    dados.setUsername(announcement.getString(ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK));
                                    dados.setEndereco(announcement.getString(ClassHunterContract.Usuario.COLUMN_ENDERECO_USUARIO));
                                    dados.setBairro(announcement.getString(ClassHunterContract.Usuario.COLUMN_BAIRRO_USUARIO));
                                    dados.setCidade(announcement.getString(ClassHunterContract.Usuario.COLUMN_CIDADE_USUARIO));
                                    dados.setTelefone(announcement.getString(ClassHunterContract.Usuario.COLUMN_TELEFONE_USUARIO));

                                    String imageEncoded = announcement.getString(ClassHunterContract.Usuario.COLUMN_IMAGEM_USUARIO);
                                    dados.setImagemPerfil(Base64.decode(imageEncoded, Base64.DEFAULT));

                                    adpAnuncios.add(dados);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            requestQueue.add(jsonObjectRequest);
        } else { // Avaliações do usuário atual pesquisadas no banco de dados interno (atualizado assim que o usuario faz login)
            // Aqui busca no banco de dados interno
            /*ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
            dao.open("read");
            adpAnuncios = dao.getAnuncios(this, email_usuario, "usuario");
            //meusAnuncioss.setAdapter(adpAnuncios);
            dao.close();*/
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_criarAnuncio:
                Intent acessarServicos = new Intent(MeusAnuncioss.this, Anuncio.class);
                acessarServicos.putExtra("email_usuario", email_usuario);
                startActivity(acessarServicos);
                break;
            case R.id.bt_menu:
                Intent acessarMenu = new Intent(MeusAnuncioss.this, Servicos.class);
                acessarMenu.putExtra("email_usuario", email_usuario);
                startActivity(acessarMenu);
                break;
            case R.id.bt_OKSelecionaAnuncio:
                String seleciona = selecionaAnuncios.getItemAtPosition(selecionaAnuncios.getSelectedItemPosition()).toString();

                if (seleciona.equals("Todos os Anúncios")) { // mostra todos os anuncios
                    getAnuncios("geral");
                } else {
                    getAnuncios("usuario");
                }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DataAnuncio dados = adpAnuncios.getItem(position);

        Intent info = new Intent(MeusAnuncioss.this, ShowAnuncio.class);
        info.putExtra("DataAnuncio",dados);
        startActivity(info);
    }
}
