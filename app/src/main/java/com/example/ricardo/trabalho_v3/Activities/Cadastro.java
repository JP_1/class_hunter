package com.example.ricardo.trabalho_v3.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ricardo.trabalho_v3.DataBase.ClassHunterContract;
import com.example.ricardo.trabalho_v3.DataBase.ClassHunterDAO;
import com.example.ricardo.trabalho_v3.R;

public class Cadastro extends AppCompatActivity implements View.OnClickListener{

    private Button proximoCad1;

    private EditText ed_username;
    private EditText ed_email;
    private EditText ed_nome;
    private EditText ed_sobrenome;
    private EditText ed_senha;
    private EditText ed_senhaRepetida;
    private boolean up_data;
    private String email_usuario;

    /* TODO:  2) salvar hash da senha no banco*
    * TODO:  3) nao deveria ter nome e sobrenome no banco */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro);

        ed_username = (EditText) findViewById(R.id.txt_nomeUsuario);
        ed_email = (EditText) findViewById(R.id.txt_email);
        ed_nome = (EditText) findViewById(R.id.txt_nome);
        ed_sobrenome = (EditText) findViewById(R.id.txt_sobrenome);
        ed_senha = (EditText) findViewById(R.id.txt_senha);
        ed_senhaRepetida = (EditText) findViewById(R.id.txt_senhaRepetida);

        up_data = this.getIntent().getBooleanExtra("update_data",false);

        //Se tela de cadastro foi acionada pelo menu principal -> atualizacao de dados
        if(up_data) {
            email_usuario = this.getIntent().getExtras().getString("email_usuario");
            TextView tv_title = (TextView) findViewById(R.id.lbl_1passo);
            tv_title.setText("Alteração de dados cadastrais");
            loadData();
        }

        proximoCad1 = (Button)findViewById(R.id.bt_proximoCad1);
        proximoCad1.setOnClickListener(this);
    }

    protected  void loadData() {
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext());
        dao.open("read");
        Cursor c = dao.getUserDataByEmail(email_usuario);
        System.out.println("treta aqui: " + email_usuario);
        String col_names[] = c.getColumnNames();
        c.moveToFirst();

        for (int i = 0; i < col_names.length; i++) {
            switch (col_names[i]) {
                case ClassHunterContract.Usuario.COLUMN_USERNAME_USUARIO_PK:
                    ed_username.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    disableEditText(ed_username);
                    break;
                case ClassHunterContract.Usuario.COLUMN_NOME_USUARIO:
                    ed_nome.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    disableEditText(ed_nome);
                    break;
                case ClassHunterContract.Usuario.COLUMN_SOBRENOME_USUARIO:
                    ed_sobrenome.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    disableEditText(ed_sobrenome);
                case ClassHunterContract.Usuario.COLUMN_EMAIL_USUARIO:
                    ed_email.setText(c.getString(c.getColumnIndexOrThrow(col_names[i])),
                            TextView.BufferType.NORMAL);
                    disableEditText(ed_email);
                    break;
                case ClassHunterContract.Usuario.COLUMN_SENHA_USUARIO:
                    ed_senha.setText(c.getString(c.getColumnIndex(col_names[i])),
                            TextView.BufferType.NORMAL);
                    ed_senhaRepetida.setText(c.getString(c.getColumnIndex(col_names[i])),
                            TextView.BufferType.NORMAL);
                default:
                    break;
            }
        }
        dao.close();
    }

    // code took from: http://stackoverflow.com/questions/4297763/disabling-of-edittext-in-android
    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setTextColor(Color.DKGRAY);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        //editText.setBackgroundColor(Color.);
    }

    private boolean isUnique(String attrToCheck, String attrValue){
        boolean unique = true;
        ClassHunterDAO dao = new ClassHunterDAO(getApplicationContext()); // o parâmetro do construtor estava getApplication
        Cursor c;

        dao.open("read");
        switch (attrToCheck){
            case "email":
                c = dao.getUserDataByEmail(attrValue);
                c.moveToFirst();
                if(c.getCount() != 0){
                    unique = false;
                }
                break;
            case "username":
                c = dao.getUserDataByUsername(attrValue);
                c.moveToFirst();
                if(c.getCount() !=0){
                    unique = false;
                }
                break;
            default:
                break;
          }
        return unique;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_proximoCad1:
                boolean valida_email = true;
                boolean valida_email2 = true;
                boolean valida_username = true;
                boolean valida_username2 = true;
                boolean valida_senha = true;
                boolean valida_senha2 = true;
                boolean valida_nome = true;
                boolean valida_sobrenome = true;

                String aux_email = ed_email.getText().toString();
                String aux_username = ed_username.getText().toString();
                String aux_senha = ed_senha.getText().toString();
                String aux_senhaRepetida = ed_senhaRepetida.getText().toString();
                String aux_nome = ed_nome.getText().toString();
                String aux_sobrenome = ed_sobrenome.getText().toString();

                valida_senha = aux_senha.equals(aux_senhaRepetida);
                if(!up_data) {
                    valida_email = isUnique("email", aux_email);
                    valida_username = isUnique("username", aux_username);
                }
                valida_nome = !aux_nome.isEmpty();
                valida_sobrenome = !aux_sobrenome.isEmpty();
                valida_username2 = !aux_username.isEmpty();
                valida_email2 = !aux_email.isEmpty();
                valida_senha2 = !aux_senha.isEmpty();

                if(valida_senha && valida_email && valida_username && valida_nome && valida_sobrenome &&
                        valida_username2 && valida_email2 && valida_senha2) {
                    Intent i = new Intent(Cadastro.this, Cadastro2.class);
                    i.putExtra("email", aux_email);
                    i.putExtra("username", aux_username);
                    i.putExtra("nome",aux_nome);
                    i.putExtra("sobrenome",aux_sobrenome);
                    i.putExtra("senha",aux_senha);
                    if (up_data) {
                        i.putExtra("update_data", up_data);
                        i.putExtra("email_usuario", email_usuario);
                    }
                    startActivity(i);
                }else{
                    if(!valida_username2) {
                        Toast.makeText(getApplicationContext(), "Campo 'Username' vazio, favor preenchê-lo",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_username){
                        Toast.makeText(getApplicationContext(),"Username já cadastro, insira outro",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_email2){
                        Toast.makeText(getApplicationContext(), "Campo 'Email' vazio, favor preenchê-lo",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_email){
                        Toast.makeText(getApplicationContext(),"E-mail já cadastrado, insira outro",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_nome){
                        Toast.makeText(getApplicationContext(),"Campo 'Nome' vazio, favor preenche-lo",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_sobrenome){
                        Toast.makeText(getApplicationContext(),"Campo 'Sobrenome' vazio, favor preenche-lo",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_senha2){
                        Toast.makeText(getApplicationContext(), "Campo 'Senha' vazio, favor preenchê-lo",
                                Toast.LENGTH_LONG).show();
                    }else if(!valida_senha){
                        Toast.makeText(getApplicationContext(),"Campos de entrada da senha não idênticos",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed(){}
}
